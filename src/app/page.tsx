"use client";

import Image from "next/image";
import styles from "./page.module.css";
import {
  Button,
  Calendar,
  Card,
  Checkbox,
  Column,
  DatePicker,
  Drawer,
  Icon,
  IconButton,
  Page,
  Select,
  Sidebar,
  Switch,
  Table,
  TableView,
  TextArea,
  TextField,
} from "@moviri/spindle-components";
import { use, useState } from "react";

export default function Home() {
  const [isUserDrawerOpen, setIsUserDrawerOpen] = useState(false);
  const options = [
    { value: "red", name: "Red" },
    { value: "blue", name: "Blue" },
    { value: "yellow", name: "Yellow" },
  ];

  const columns = [
    { name: "User", key: "user", width: 400 },
    { name: "Username", key: "username", width: 400 },
    { name: "Email", key: "email" },
    { name: "Type", key: "type" },
    { name: "Roles", key: "roles" },
    { name: "", key: "actions" },
  ];

  /*   const tableItems: {
    id: number;
    user: JSX.Element;
    username: string;
    email: string;
    type: string;
    roles: string;
    actions?: JSX.Element;
  }[] = [
    {
      id: 1,
      user: <>Jese Leos </>,
      username: "jese",
      email: "email@gmail.com",
      type: "admin",
      roles: "R | W",
      actions: (
        <>
          <IconButton
            symbol="arrow_forward"
            onClick={() => setIsUserDrawerOpen(true)}
            placeholder=""
            onPointerEnterCapture={() => {}}
            onPointerLeaveCapture={() => {}}
          />
        </>
      ),
    },
    {
      id: 2,
      user: <>Bonnie Green</>,
      username: "bonnie",
      email: "email@gmail.com",
      type: "",
      roles: "R",
      actions: (
        <>
          <IconButton
            symbol="arrow_forward"
            onClick={() => setIsUserDrawerOpen(true)}
          />
        </>
      ),
    },
    {
      id: 3,
      user: <>Leslie Livingstone</>,
      username: "leslie",
      email: "email@gmail.com",
      type: "",
      roles: "R | W",
      actions: (
        <>
          <IconButton
            symbol="arrow_forward"
            onClick={() => setIsUserDrawerOpen(true)}
          />
        </>
      ),
    },
    {
      id: 4,
      user: <>Michael Gough</>,
      username: "michael",
      email: "email@gmail.com",
      type: "",
      roles: "R",
      actions: (
        <>
          <IconButton
            symbol="arrow_forward"
            onClick={() => setIsUserDrawerOpen(true)}
          />
        </>
      ),
    },
  ];
 */
  return (
    <main className={styles.main}>
      <Page>
        <Page.Header>
          <Page.Title>Account</Page.Title>
        </Page.Header>

        <Page.Sidebar>
          <Sidebar>
            <Sidebar.Item
              icon={<Icon symbol="account_circle" />}
              label="Account"
              selected
            />
            <Sidebar.Item icon={<Icon symbol="home" />} label="Products" />
            <Sidebar.Item icon={<Icon symbol="checkroom" />} label="Products" />
          </Sidebar>
        </Page.Sidebar>
        <Page.Content>
          <Card>
            <Card.Title>Account Information</Card.Title>
            <Card.Actions>
              <Button primary>Primary Button</Button>
              <Button secondary>Save Changes</Button>
              <Button text>Text Button</Button>
            </Card.Actions>
            <Card.Body>
              <Column gap={1}>
                <Button primary>
                  <Icon symbol="add" />
                  Click me
                </Button>

                <IconButton
                  symbol="add"
                  placeholder=""
                  /* onPointerEnterCapture={() => {}}
                  onPointerLeaveCapture={() => {}} */
                />
                <Checkbox>Check me</Checkbox>
                {/*  <Switch>Switch me</Switch> */}
                <TextField label="Name" placeholder="Write here your name" />
                <TextField
                  label="Surname"
                  placeholder="Write here your surname"
                />
                <TextField
                  label="Email"
                  placeholder="Write here your email"
                  type="email"
                />
                <TextArea label="Description" placeholder="Write here..." />
                <Select placeholder="Select" items={options} label="Country">
                  {({ value, name }: { value: string; name: string }) => (
                    <Select.Option key={value}>{name}</Select.Option>
                  )}
                </Select>
                <DatePicker locale="en" />
              </Column>
            </Card.Body>
          </Card>
          <Card>
            <Card.Title>Users List</Card.Title>
            <Card.Actions>
              <Button primary>Add user</Button>
            </Card.Actions>
            {/*   <Card.Body>
              <TableView selectionMode="multiple">
                <Table.Header columns={columns}>
                  {({ name, ...props }) => (
                    <Table.Column {...props}>{name}</Table.Column>
                  )}
                </Table.Header>
                <Table.Body items={tableItems}>
                  {(item: {
                    id: number;
                    user: JSX.Element;
                    username: string;
                    email: string;
                    type: string;
                    roles: string;
                    actions?: JSX.Element;
                  }) => (
                    <Table.Row>
                      {(columnKey) => (
                        <Table.Cell>{item[columnKey]}</Table.Cell>
                      )}
                    </Table.Row>
                  )}
                </Table.Body>
              </TableView>
            </Card.Body> */}
          </Card>
        </Page.Content>
        <Page.Footer>
          Copyright © 2024 Moviri S.p.A. All rights reserved.
        </Page.Footer>
      </Page>
      <Drawer
        isOpen={isUserDrawerOpen}
        onIsOpenChange={(open) => {
          setIsUserDrawerOpen(open);
        }}
      ></Drawer>
    </main>
  );
}
