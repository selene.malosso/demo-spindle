/**
 * Do not edit directly
 * Generated on Tue, 02 Apr 2024 09:31:07 GMT
 */

export const coreColorPrimarySource = "var(--core-color-primary-source)";
export const coreColorNeutralSource = "var(--core-color-neutral-source)";
export const coreColorPrimary100 = "var(--core-color-primary-100)";
export const coreColorPrimary99 = "var(--core-color-primary-99)";
export const coreColorPrimary98 = "var(--core-color-primary-98)";
export const coreColorPrimary95 = "var(--core-color-primary-95)";
export const coreColorPrimary90 = "var(--core-color-primary-90)";
export const coreColorPrimary80 = "var(--core-color-primary-80)";
export const coreColorPrimary70 = "var(--core-color-primary-70)";
export const coreColorPrimary60 = "var(--core-color-primary-60)";
export const coreColorPrimary50 = "var(--core-color-primary-50)";
export const coreColorPrimary40 = "var(--core-color-primary-40)";
export const coreColorPrimary35 = "var(--core-color-primary-35)";
export const coreColorPrimary30 = "var(--core-color-primary-30)";
export const coreColorPrimary25 = "var(--core-color-primary-25)";
export const coreColorPrimary20 = "var(--core-color-primary-20)";
export const coreColorPrimary10 = "var(--core-color-primary-10)";
export const coreColorPrimary0 = "var(--core-color-primary-0)";
export const coreColorNeutral100 = "var(--core-color-neutral-100)";
export const coreColorNeutral99 = "var(--core-color-neutral-99)";
export const coreColorNeutral98 = "var(--core-color-neutral-98)";
export const coreColorNeutral95 = "var(--core-color-neutral-95)";
export const coreColorNeutral90 = "var(--core-color-neutral-90)";
export const coreColorNeutral80 = "var(--core-color-neutral-80)";
export const coreColorNeutral70 = "var(--core-color-neutral-70)";
export const coreColorNeutral60 = "var(--core-color-neutral-60)";
export const coreColorNeutral50 = "var(--core-color-neutral-50)";
export const coreColorNeutral40 = "var(--core-color-neutral-40)";
export const coreColorNeutral35 = "var(--core-color-neutral-35)";
export const coreColorNeutral30 = "var(--core-color-neutral-30)";
export const coreColorNeutral25 = "var(--core-color-neutral-25)";
export const coreColorNeutral20 = "var(--core-color-neutral-20)";
export const coreColorNeutral10 = "var(--core-color-neutral-10)";
export const coreColorNeutral0 = "var(--core-color-neutral-0)";
export const coreDimensionDimension25 = "var(--core-dimension-dimension-25)";
export const coreDimensionDimension50 = "var(--core-dimension-dimension-50)";
export const coreDimensionDimension100 = "var(--core-dimension-dimension-100)";
export const coreDimensionDimension200 = "var(--core-dimension-dimension-200)";
export const coreDimensionDimension300 = "var(--core-dimension-dimension-300)";
export const coreDimensionDimension400 = "var(--core-dimension-dimension-400)";
export const coreTransitionLinearDuration = "var(--core-transition-linear-duration)";
export const coreTransitionLinearTimingFunction = "var(--core-transition-linear-timing-function)";
export const coreTypographyFontSizeBase = "var(--core-typography-font-size-base)";
export const coreTypographyFontSizeScale = "var(--core-typography-font-size-scale)";
export const coreTypographyFontSize25 = "var(--core-typography-font-size-25)";
export const coreTypographyFontSize50 = "var(--core-typography-font-size-50)";
export const coreTypographyFontSize75 = "var(--core-typography-font-size-75)";
export const coreTypographyFontSize100 = "var(--core-typography-font-size-100)";
export const coreTypographyFontSize200 = "var(--core-typography-font-size-200)";
export const coreTypographyFontSize300 = "var(--core-typography-font-size-300)";
export const coreTypographyFontSize400 = "var(--core-typography-font-size-400)";
export const coreTypographyFontSize500 = "var(--core-typography-font-size-500)";
export const coreTypographyFontSize600 = "var(--core-typography-font-size-600)";
export const coreTypographyFontSize700 = "var(--core-typography-font-size-700)";
export const coreTypographyFontSize800 = "var(--core-typography-font-size-800)";
export const coreTypographyFontSize900 = "var(--core-typography-font-size-900)";
export const coreTypographyFontSize1000 = "var(--core-typography-font-size-1000)";
export const coreTypographyFontSize1100 = "var(--core-typography-font-size-1100)";
export const coreTypographyFontSize1200 = "var(--core-typography-font-size-1200)";
export const coreTypographyFontSize1300 = "var(--core-typography-font-size-1300)";
export const coreTypographyFontSize1400 = "var(--core-typography-font-size-1400)";
export const coreTypographyWeightNormal = "var(--core-typography-weight-normal)";
export const coreTypographyWeightMedium = "var(--core-typography-weight-medium)";
export const coreTypographyFont = "var(--core-typography-font)";
export const semDimensionComponentSmallHeight = "var(--sem-dimension-component-small-height)";
export const semDimensionComponentSmallWidth = "var(--sem-dimension-component-small-width)";
export const semDimensionComponentSmallBorderRadius = "var(--sem-dimension-component-small-border-radius)";
export const semDimensionComponentSmallBorderWidth = "var(--sem-dimension-component-small-border-width)";
export const semDimensionComponentSmallPaddingHorizontalDefault = "var(--sem-dimension-component-small-padding-horizontal-default)";
export const semDimensionComponentSmallPaddingHorizontalLeadingIcon = "var(--sem-dimension-component-small-padding-horizontal-leading-icon)";
export const semDimensionComponentSmallPaddingHorizontalTrailingIcon = "var(--sem-dimension-component-small-padding-horizontal-trailing-icon)";
export const semDimensionComponentMediumBorderRadius = "var(--sem-dimension-component-medium-border-radius)";
export const semDimensionComponentMediumBorderWidth = "var(--sem-dimension-component-medium-border-width)";
export const semOpacityComponentDisabled = "var(--sem-opacity-component-disabled)";
export const semOpacityTintHover = "var(--sem-opacity-tint-hover)";
export const semOpacityTintActive = "var(--sem-opacity-tint-active)";
export const semOpacityTintDisabled = "var(--sem-opacity-tint-disabled)";
export const semOpacityTextHighEmphasis = "var(--sem-opacity-text-high-emphasis)";
export const semOpacityTextMediumEmphasis = "var(--sem-opacity-text-medium-emphasis)";
export const semOpacityTextDisabled = "var(--sem-opacity-text-disabled)";
export const semTypographyButtonFont = "var(--sem-typography-button-font)";
export const semTypographyButtonWeight = "var(--sem-typography-button-weight)";
export const semTypographyButtonSize = "var(--sem-typography-button-size)";
export const semTypographyButtonLineHeight = "var(--sem-typography-button-line-height)";
export const semTypographyButtonLetterSpacing = "var(--sem-typography-button-letter-spacing)";
export const semTypographyHeadlineLargeFont = "var(--sem-typography-headline-large-font)";
export const semTypographyHeadlineLargeSize = "var(--sem-typography-headline-large-size)";
export const semTypographyHeadlineLargeWeight = "var(--sem-typography-headline-large-weight)";
export const semTypographyHeadlineLargeLineHeight = "var(--sem-typography-headline-large-line-height)";
export const semTypographyHeadlineLargeLetterSpacing = "var(--sem-typography-headline-large-letter-spacing)";
export const semTypographyHeadlineMediumFont = "var(--sem-typography-headline-medium-font)";
export const semTypographyHeadlineMediumSize = "var(--sem-typography-headline-medium-size)";
export const semTypographyHeadlineMediumWeight = "var(--sem-typography-headline-medium-weight)";
export const semTypographyHeadlineMediumLineHeight = "var(--sem-typography-headline-medium-line-height)";
export const semTypographyHeadlineMediumLetterSpacing = "var(--sem-typography-headline-medium-letter-spacing)";
export const semTypographyHeadlineSmallFont = "var(--sem-typography-headline-small-font)";
export const semTypographyHeadlineSmallSize = "var(--sem-typography-headline-small-size)";
export const semTypographyHeadlineSmallWeight = "var(--sem-typography-headline-small-weight)";
export const semTypographyHeadlineSmallLineHeight = "var(--sem-typography-headline-small-line-height)";
export const semTypographyHeadlineSmallLetterSpacing = "var(--sem-typography-headline-small-letter-spacing)";
export const semTypographyTitleLargeFont = "var(--sem-typography-title-large-font)";
export const semTypographyTitleLargeSize = "var(--sem-typography-title-large-size)";
export const semTypographyTitleLargeWeight = "var(--sem-typography-title-large-weight)";
export const semTypographyTitleLargeLineHeight = "var(--sem-typography-title-large-line-height)";
export const semTypographyTitleLargeLetterSpacing = "var(--sem-typography-title-large-letter-spacing)";
export const semTypographyTitleMediumFont = "var(--sem-typography-title-medium-font)";
export const semTypographyTitleMediumSize = "var(--sem-typography-title-medium-size)";
export const semTypographyTitleMediumWeight = "var(--sem-typography-title-medium-weight)";
export const semTypographyTitleMediumLineHeight = "var(--sem-typography-title-medium-line-height)";
export const semTypographyTitleMediumLetterSpacing = "var(--sem-typography-title-medium-letter-spacing)";
export const semTypographyTitleSmallFont = "var(--sem-typography-title-small-font)";
export const semTypographyTitleSmallSize = "var(--sem-typography-title-small-size)";
export const semTypographyTitleSmallWeight = "var(--sem-typography-title-small-weight)";
export const semTypographyTitleSmallLineHeight = "var(--sem-typography-title-small-line-height)";
export const semTypographyTitleSmallLetterSpacing = "var(--sem-typography-title-small-letter-spacing)";
export const semTypographySubtitleLargeFont = "var(--sem-typography-subtitle-large-font)";
export const semTypographySubtitleLargeSize = "var(--sem-typography-subtitle-large-size)";
export const semTypographySubtitleLargeWeight = "var(--sem-typography-subtitle-large-weight)";
export const semTypographySubtitleLargeLineHeight = "var(--sem-typography-subtitle-large-line-height)";
export const semTypographySubtitleLargeLetterSpacing = "var(--sem-typography-subtitle-large-letter-spacing)";
export const semTypographySubtitleMediumFont = "var(--sem-typography-subtitle-medium-font)";
export const semTypographySubtitleMediumSize = "var(--sem-typography-subtitle-medium-size)";
export const semTypographySubtitleMediumWeight = "var(--sem-typography-subtitle-medium-weight)";
export const semTypographySubtitleMediumLineHeight = "var(--sem-typography-subtitle-medium-line-height)";
export const semTypographySubtitleMediumLetterSpacing = "var(--sem-typography-subtitle-medium-letter-spacing)";
export const semTypographySubtitleSmallFont = "var(--sem-typography-subtitle-small-font)";
export const semTypographySubtitleSmallSize = "var(--sem-typography-subtitle-small-size)";
export const semTypographySubtitleSmallWeight = "var(--sem-typography-subtitle-small-weight)";
export const semTypographySubtitleSmallLineHeight = "var(--sem-typography-subtitle-small-line-height)";
export const semTypographySubtitleSmallLetterSpacing = "var(--sem-typography-subtitle-small-letter-spacing)";
export const semTypographyBodyLargeFont = "var(--sem-typography-body-large-font)";
export const semTypographyBodyLargeSize = "var(--sem-typography-body-large-size)";
export const semTypographyBodyLargeWeight = "var(--sem-typography-body-large-weight)";
export const semTypographyBodyLargeLineHeight = "var(--sem-typography-body-large-line-height)";
export const semTypographyBodyLargeLetterSpacing = "var(--sem-typography-body-large-letter-spacing)";
export const semTypographyBodyMediumFont = "var(--sem-typography-body-medium-font)";
export const semTypographyBodyMediumSize = "var(--sem-typography-body-medium-size)";
export const semTypographyBodyMediumWeight = "var(--sem-typography-body-medium-weight)";
export const semTypographyBodyMediumLineHeight = "var(--sem-typography-body-medium-line-height)";
export const semTypographyBodyMediumLetterSpacing = "var(--sem-typography-body-medium-letter-spacing)";
export const semTypographyBodySmallFont = "var(--sem-typography-body-small-font)";
export const semTypographyBodySmallSize = "var(--sem-typography-body-small-size)";
export const semTypographyBodySmallWeight = "var(--sem-typography-body-small-weight)";
export const semTypographyBodySmallLineHeight = "var(--sem-typography-body-small-line-height)";
export const semTypographyBodySmallLetterSpacing = "var(--sem-typography-body-small-letter-spacing)";
export const semTypographyLabelLargeFont = "var(--sem-typography-label-large-font)";
export const semTypographyLabelLargeSize = "var(--sem-typography-label-large-size)";
export const semTypographyLabelLargeWeight = "var(--sem-typography-label-large-weight)";
export const semTypographyLabelLargeLineHeight = "var(--sem-typography-label-large-line-height)";
export const semTypographyLabelLargeLetterSpacing = "var(--sem-typography-label-large-letter-spacing)";
export const semTypographyLabelMediumFont = "var(--sem-typography-label-medium-font)";
export const semTypographyLabelMediumSize = "var(--sem-typography-label-medium-size)";
export const semTypographyLabelMediumWeight = "var(--sem-typography-label-medium-weight)";
export const semTypographyLabelMediumLineHeight = "var(--sem-typography-label-medium-line-height)";
export const semTypographyLabelMediumLetterSpacing = "var(--sem-typography-label-medium-letter-spacing)";
export const semTypographyLabelSmallFont = "var(--sem-typography-label-small-font)";
export const semTypographyLabelSmallSize = "var(--sem-typography-label-small-size)";
export const semTypographyLabelSmallWeight = "var(--sem-typography-label-small-weight)";
export const semTypographyLabelSmallLineHeight = "var(--sem-typography-label-small-line-height)";
export const semTypographyLabelSmallLetterSpacing = "var(--sem-typography-label-small-letter-spacing)";
export const semColorBackgroundPage = "var(--sem-color-background-page)";
export const semColorBackgroundComponentDefault = "var(--sem-color-background-component-default)";
export const semColorBackgroundSurfaceDefault = "var(--sem-color-background-surface-default)";
export const semColorBackgroundPrimaryDefault = "var(--sem-color-background-primary-default)";
export const semColorBackgroundPrimaryHover = "var(--sem-color-background-primary-hover)";
export const semColorBackgroundPrimaryPressed = "var(--sem-color-background-primary-pressed)";
export const semColorBackgroundPrimaryDisabled = "var(--sem-color-background-primary-disabled)";
export const semColorBackgroundSecondaryPressed = "var(--sem-color-background-secondary-pressed)";
export const semColorBorderSecondaryDefault = "var(--sem-color-border-secondary-default)";
export const semColorBorderSecondaryHover = "var(--sem-color-border-secondary-hover)";
export const semColorBorderSecondaryPressed = "var(--sem-color-border-secondary-pressed)";
export const semColorBorderSecondaryDisabled = "var(--sem-color-border-secondary-disabled)";
export const semColorBorderSurfaceSmallDefault = "var(--sem-color-border-surface-small-default)";
export const semColorBorderSurfaceMediumDefault = "var(--sem-color-border-surface-medium-default)";
export const semColorTextOnBackgroundDefault = "var(--sem-color-text-on-background-default)";
export const semColorTextOnSurfaceDefault = "var(--sem-color-text-on-surface-default)";
export const semColorTextPrimaryDefault = "var(--sem-color-text-primary-default)";
export const semColorTextPrimaryHover = "var(--sem-color-text-primary-hover)";
export const semColorTextPrimaryPressed = "var(--sem-color-text-primary-pressed)";
export const semColorTextPrimaryDisabled = "var(--sem-color-text-primary-disabled)";
export const semColorTextOnPrimaryDefault = "var(--sem-color-text-on-primary-default)";
export const semColorTextOnPrimaryHover = "var(--sem-color-text-on-primary-hover)";
export const semColorTextOnPrimaryPressed = "var(--sem-color-text-on-primary-pressed)";
export const semColorTextSecondaryDefault = "var(--sem-color-text-secondary-default)";
export const semColorTextSecondaryHover = "var(--sem-color-text-secondary-hover)";
export const semColorTextSecondaryPressed = "var(--sem-color-text-secondary-pressed)";
export const semColorTextSecondaryDisabled = "var(--sem-color-text-secondary-disabled)";
