/**
 * Do not edit directly
 * Generated on Tue, 02 Apr 2024 09:31:07 GMT
 */

export const coreColorPrimarySource : string;
export const coreColorNeutralSource : string;
export const coreColorPrimary100 : string;
export const coreColorPrimary99 : string;
export const coreColorPrimary98 : string;
export const coreColorPrimary95 : string;
export const coreColorPrimary90 : string;
export const coreColorPrimary80 : string;
export const coreColorPrimary70 : string;
export const coreColorPrimary60 : string;
export const coreColorPrimary50 : string;
export const coreColorPrimary40 : string;
export const coreColorPrimary35 : string;
export const coreColorPrimary30 : string;
export const coreColorPrimary25 : string;
export const coreColorPrimary20 : string;
export const coreColorPrimary10 : string;
export const coreColorPrimary0 : string;
export const coreColorNeutral100 : string;
export const coreColorNeutral99 : string;
export const coreColorNeutral98 : string;
export const coreColorNeutral95 : string;
export const coreColorNeutral90 : string;
export const coreColorNeutral80 : string;
export const coreColorNeutral70 : string;
export const coreColorNeutral60 : string;
export const coreColorNeutral50 : string;
export const coreColorNeutral40 : string;
export const coreColorNeutral35 : string;
export const coreColorNeutral30 : string;
export const coreColorNeutral25 : string;
export const coreColorNeutral20 : string;
export const coreColorNeutral10 : string;
export const coreColorNeutral0 : string;
export const coreDimensionDimension25 : string;
export const coreDimensionDimension50 : string;
export const coreDimensionDimension100 : string;
export const coreDimensionDimension200 : string;
export const coreDimensionDimension300 : string;
export const coreDimensionDimension400 : string;
export const coreTransitionLinearDuration : string;
export const coreTransitionLinearTimingFunction : string;
export const coreTypographyFontSizeBase : string;
export const coreTypographyFontSizeScale : string;
export const coreTypographyFontSize25 : string;
export const coreTypographyFontSize50 : string;
export const coreTypographyFontSize75 : string;
export const coreTypographyFontSize100 : string;
export const coreTypographyFontSize200 : string;
export const coreTypographyFontSize300 : string;
export const coreTypographyFontSize400 : string;
export const coreTypographyFontSize500 : string;
export const coreTypographyFontSize600 : string;
export const coreTypographyFontSize700 : string;
export const coreTypographyFontSize800 : string;
export const coreTypographyFontSize900 : string;
export const coreTypographyFontSize1000 : string;
export const coreTypographyFontSize1100 : string;
export const coreTypographyFontSize1200 : string;
export const coreTypographyFontSize1300 : string;
export const coreTypographyFontSize1400 : string;
export const coreTypographyWeightNormal : string;
export const coreTypographyWeightMedium : string;
export const coreTypographyFont : string;
export const semDimensionComponentSmallHeight : string;
export const semDimensionComponentSmallWidth : string;
export const semDimensionComponentSmallBorderRadius : string;
export const semDimensionComponentSmallBorderWidth : string;
export const semDimensionComponentSmallPaddingHorizontalDefault : string;
export const semDimensionComponentSmallPaddingHorizontalLeadingIcon : string;
export const semDimensionComponentSmallPaddingHorizontalTrailingIcon : string;
export const semDimensionComponentMediumBorderRadius : string;
export const semDimensionComponentMediumBorderWidth : string;
export const semOpacityComponentDisabled : string;
export const semOpacityTintHover : string;
export const semOpacityTintActive : string;
export const semOpacityTintDisabled : string;
export const semOpacityTextHighEmphasis : string;
export const semOpacityTextMediumEmphasis : string;
export const semOpacityTextDisabled : string;
export const semTypographyButtonFont : string;
export const semTypographyButtonWeight : string;
export const semTypographyButtonSize : string;
export const semTypographyButtonLineHeight : string;
export const semTypographyButtonLetterSpacing : string;
export const semTypographyHeadlineLargeFont : string;
export const semTypographyHeadlineLargeSize : string;
export const semTypographyHeadlineLargeWeight : string;
export const semTypographyHeadlineLargeLineHeight : string;
export const semTypographyHeadlineLargeLetterSpacing : string;
export const semTypographyHeadlineMediumFont : string;
export const semTypographyHeadlineMediumSize : string;
export const semTypographyHeadlineMediumWeight : string;
export const semTypographyHeadlineMediumLineHeight : string;
export const semTypographyHeadlineMediumLetterSpacing : string;
export const semTypographyHeadlineSmallFont : string;
export const semTypographyHeadlineSmallSize : string;
export const semTypographyHeadlineSmallWeight : string;
export const semTypographyHeadlineSmallLineHeight : string;
export const semTypographyHeadlineSmallLetterSpacing : string;
export const semTypographyTitleLargeFont : string;
export const semTypographyTitleLargeSize : string;
export const semTypographyTitleLargeWeight : string;
export const semTypographyTitleLargeLineHeight : string;
export const semTypographyTitleLargeLetterSpacing : string;
export const semTypographyTitleMediumFont : string;
export const semTypographyTitleMediumSize : string;
export const semTypographyTitleMediumWeight : string;
export const semTypographyTitleMediumLineHeight : string;
export const semTypographyTitleMediumLetterSpacing : string;
export const semTypographyTitleSmallFont : string;
export const semTypographyTitleSmallSize : string;
export const semTypographyTitleSmallWeight : string;
export const semTypographyTitleSmallLineHeight : string;
export const semTypographyTitleSmallLetterSpacing : string;
export const semTypographySubtitleLargeFont : string;
export const semTypographySubtitleLargeSize : string;
export const semTypographySubtitleLargeWeight : string;
export const semTypographySubtitleLargeLineHeight : string;
export const semTypographySubtitleLargeLetterSpacing : string;
export const semTypographySubtitleMediumFont : string;
export const semTypographySubtitleMediumSize : string;
export const semTypographySubtitleMediumWeight : string;
export const semTypographySubtitleMediumLineHeight : string;
export const semTypographySubtitleMediumLetterSpacing : string;
export const semTypographySubtitleSmallFont : string;
export const semTypographySubtitleSmallSize : string;
export const semTypographySubtitleSmallWeight : string;
export const semTypographySubtitleSmallLineHeight : string;
export const semTypographySubtitleSmallLetterSpacing : string;
export const semTypographyBodyLargeFont : string;
export const semTypographyBodyLargeSize : string;
export const semTypographyBodyLargeWeight : string;
export const semTypographyBodyLargeLineHeight : string;
export const semTypographyBodyLargeLetterSpacing : string;
export const semTypographyBodyMediumFont : string;
export const semTypographyBodyMediumSize : string;
export const semTypographyBodyMediumWeight : string;
export const semTypographyBodyMediumLineHeight : string;
export const semTypographyBodyMediumLetterSpacing : string;
export const semTypographyBodySmallFont : string;
export const semTypographyBodySmallSize : string;
export const semTypographyBodySmallWeight : string;
export const semTypographyBodySmallLineHeight : string;
export const semTypographyBodySmallLetterSpacing : string;
export const semTypographyLabelLargeFont : string;
export const semTypographyLabelLargeSize : string;
export const semTypographyLabelLargeWeight : string;
export const semTypographyLabelLargeLineHeight : string;
export const semTypographyLabelLargeLetterSpacing : string;
export const semTypographyLabelMediumFont : string;
export const semTypographyLabelMediumSize : string;
export const semTypographyLabelMediumWeight : string;
export const semTypographyLabelMediumLineHeight : string;
export const semTypographyLabelMediumLetterSpacing : string;
export const semTypographyLabelSmallFont : string;
export const semTypographyLabelSmallSize : string;
export const semTypographyLabelSmallWeight : string;
export const semTypographyLabelSmallLineHeight : string;
export const semTypographyLabelSmallLetterSpacing : string;
export const semColorBackgroundPage : string;
export const semColorBackgroundComponentDefault : string;
export const semColorBackgroundSurfaceDefault : string;
export const semColorBackgroundPrimaryDefault : string;
export const semColorBackgroundPrimaryHover : string;
export const semColorBackgroundPrimaryPressed : string;
export const semColorBackgroundPrimaryDisabled : string;
export const semColorBackgroundSecondaryPressed : string;
export const semColorBorderSecondaryDefault : string;
export const semColorBorderSecondaryHover : string;
export const semColorBorderSecondaryPressed : string;
export const semColorBorderSecondaryDisabled : string;
export const semColorBorderSurfaceSmallDefault : string;
export const semColorBorderSurfaceMediumDefault : string;
export const semColorTextOnBackgroundDefault : string;
export const semColorTextOnSurfaceDefault : string;
export const semColorTextPrimaryDefault : string;
export const semColorTextPrimaryHover : string;
export const semColorTextPrimaryPressed : string;
export const semColorTextPrimaryDisabled : string;
export const semColorTextOnPrimaryDefault : string;
export const semColorTextOnPrimaryHover : string;
export const semColorTextOnPrimaryPressed : string;
export const semColorTextSecondaryDefault : string;
export const semColorTextSecondaryHover : string;
export const semColorTextSecondaryPressed : string;
export const semColorTextSecondaryDisabled : string;
