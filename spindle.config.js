/** @type {import("@moviri/spindle-theme").Config} */
const config = {
  platforms: [{ format: "css-theme", destination: "dist/demo.css" }],
  tokens: {
    core: {
      typography: {
        font: {
          brand: { value: "var(--Inter)" },
          plain: { value: "var(--Inter)" },
        },
      },
    },
  },
};

module.exports = config;
